package buu.thodsaporn.hellome

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import org.w3c.dom.Text

class MainActivity : AppCompatActivity(), View.OnClickListener {
    val LOG = "MAIN_ACTIVITY"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val editName = findViewById<EditText>(R.id.editName)
        val btnHello = findViewById<Button>(R.id.btnHello)
        val txtHello = findViewById<TextView>(R.id.txtHello)
//        btnHello.setOnClickListener(this)
//        btnHello.setOnClickListener(object: View.OnClickListener{
//            override fun onClick(view: View?) {
//                Toast.makeText( this@MainActivity,"Click Me", Toast.LENGTH_LONG).show()
//            }
//
//        })
        btnHello.setOnClickListener {
            Toast.makeText( this@MainActivity,"Hello " + editName.text.toString(), Toast.LENGTH_LONG).show()
            Log.d(LOG, "Click Me")
            txtHello.text = "Hello " + editName.text.toString()
        }
    }

    override fun onClick(view: View?) {
        Toast.makeText( this,"Click Me", Toast.LENGTH_LONG).show()
    }
}